# networkscanner.py

**This code is very much still work in progress. Use at your own risk.**

This script is written for python3, and relies on the module `netifaces` and `mysql.connector`.

You can install it using:

```sh
pip3 install netifaces mysql-connector-python
```

It also requite `nmap` and `arp-scan`, you can install those using your default package manager.

I have tested the code on Ubuntu 18.04 and macOS Sierra.

You will need to run the script as root, in order to succeed.

# FAQ

Yes, I know I shouldn't parse nmap output directly, and instead use the XML output. What can I say, I'm lazy so I did it this way, as I already knew how to do it.

# IEEE OUI table

The UOI table in this repo was compiled on September 7th 2018 using the [parseoui.awk](https://gist.github.com/ork/8508228) tool and further processing locally in vim. The data comes [directly from the IEEE OUI list](http://standards-oui.ieee.org/oui.txt).

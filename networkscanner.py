#!/usr/bin/env python3
# Copyright 2018 Morten Jakobsen. All rights reserved.
# Use of this source code is governed by a BSD-style
# license that can be found in the LICENSE file.

from ipaddress import ip_interface, ip_network
from subprocess import check_output
from sys import exit
from socket import gethostbyaddr
from pathlib import Path
import netifaces, logging, argparse, os, json, mysql.connector

class networkscanner:


    def __init__(self):
        """
        NetworkScanner Constructor

        :return: None
        """
        # Script path
        self.scriptPath = os.path.dirname(os.path.realpath(__file__))
        self.oui_file=self.scriptPath + '/ieee-oui.json'

        # CLI color use
        self.color = {
            'red': '\033[0;31m',
            'yellow': '\033[0;33m',
            'green': '\033[0;32m',
            'blue': '\033[0;36m',
            'nc': '\033[0m'
        }

        # container variables
        self.interfaces = []
        self.networks = []
        self.result = {}
        self.mysql = {}


    def run(self):
        """
        Execution method

        :return: result dictionary
        """
        self.getNetworks()
        self.arpScan()
        self.nmapScan()
        self.getOpenPorts()
        self.populateHostnames()
        self.populateVendors()

        return self.result


    def getNetworks(self):
        """
        Obtain the networks available

        :return: None
        """
        if self.interfaces == "all":
            self.interfaces = netifaces.interfaces()

        for interface in self.interfaces:
            logging.info("Getting network from interface: " + interface)
            ip = self.getIpOfInterface(interface)
            try:
                ip = str(ip_interface('/'.join((ip[0], ip[1]))))
            except Exception:
                logging.debug("Unknown exception ocurred in getNetworks", exc_info=True)
                ip = None

            cidr =  str(ip_network(ip, strict=False)) if ip else None

            if cidr is not None:
                logging.info("Network found: " + cidr)
                self.networks.append(cidr)

        if len(self.networks) == 0:
            logging.info("No networks found!")
            exit(1)


    def getIpOfInterface(self, interface):
        """
        Obtain IP of given interface

        :param interface: interface object
        :return: iterable of ip and netmask
        """
        try:
            netifaces.ifaddresses(interface)
            ip = netifaces.ifaddresses(interface)[netifaces.AF_INET][0]['addr']
            netmask = netifaces.ifaddresses(interface)[netifaces.AF_INET][0]['netmask']
        except Exception:
            logging.debug("Unknown exception ocurred in getIpOfInterface", exc_info=True)
            ip = None

        return [ip, netmask] if ip is not None else None


    def arpScan(self):
        """
        Create a useable self.result dictionary from an arp scan

        :return: None
        """
        for network in self.networks:
            logging.info("Scanning using ARP")
            result = check_output(["arp-scan", network]).decode()

            for line in result.splitlines():
                data = line.split('\t')

                if(len(data) == 3):
                    self.result[data[0]] = {
                        "mac": str(data[1][:17]),
                        "vendor": None,
                        "hostname": None,
                        "ports": []
                    }
        logging.info("Processed " + str(len(result)) + " lines.")


    def nmapScan(self):
        """
        Execute a nmap scan and populate self.results

        :return: None
        """
        for network in self.networks:
            logging.info("Scanning using NMAP")
            result = check_output(["nmap", network, "-n", "-sn"]).decode()

            for line in result.splitlines():
                if "scan report" in str(line):
                    #ip = re.sub(r'Nmap scan report for ', '', str(line))
                    ip = line.replace('Nmap scan report for ', '')

                    if ip not in self.result:
                        self.result[ip] = {
                            "mac": None,
                            "vendor": None,
                            "hostname": None,
                            "ports": []
                        }
        logging.info("Processed " + str(len(result)) + " lines.")


    def getOpenPorts(self):
        """
        Update self.result dictionary with open ports from nmap scan

        :return: None
        """
        logging.info("Scanning " + str(len(self.result)) + " hosts in " + str(len(self.networks)) + " networks")
        n = 0
        for ip,data in self.result.items():
            n += 1
            logging.info("Scanning host (" + str(n) + "/" + str(len(self.result)) + "): " + str(ip))
            result = check_output(["nmap", '-Pn', '--host-timeout', '30s', ip]).decode()
            postlist=False

            for line in result.splitlines():

                if "PORT" in str(line):
                    postlist=True

                if "Nmap done" in str(line):
                    postlist=False

                if postlist == True:
                    data = line.split()
                try:
                    if "/" in data[0]:
                        port = data[0].split("/")
                        self.result[ip]['ports'].append([port[0],port[1],data[1],data[2]])
                except Exception:
                    logging.debug("Unknown exception ocurred in getOpenPorts", exc_info=True)


    def populateHostnames(self):
        """
        Update self.result dictionary with hostnames from reverse IP lookups

        :return: None
        """
        logging.info("Looking up DNS")
        for ip in self.result.keys():
            try:
                dns = gethostbyaddr(ip)[0]
                self.result[ip]['hostname'] = dns
            except Exception:
                logging.debug("Unknown exception ocurred in populateHostnames", exc_info=True)
                self.result[ip]['hostname'] = None


    def populateVendors(self):
        """
        Update self.result dictionary with hostnames from OUI table

        :return: None
        """

        with open(self.oui_file) as f:
            self.oui_table = json.load(f)

        logging.info("Looking up MAC vendors")
        for ip in self.result.keys():
            if self.result[ip]['mac'] is None:
                vendor = None
            else:
                mac_vendor = self.result[ip]['mac'][0:8].replace(':', '-').upper()
                vendor = self.oui_table[mac_vendor] if mac_vendor in self.oui_table else None
            self.result[ip]['vendor'] = vendor


    def whitespace(self, size):
        """
        Generate string of whitespace

        :param size: character length of interface
        :return: string of whitespace
        """
        output = ''.join((' ' * size))
        return output


    def column(self,size,value):
        """
        Generate fixed width output

        :param size: max length of output
        :param value: text to be outputted
        :return: output with whitespace
        """
        length=size-len(value)
        if len(value) > size:
            output = value[0:size-3] + ".. "
        else:
            output = ''.join((value, ' ' * length))
        return output


    def printRedifNone(self, size, value, error):
        """
        return error string in red if value is None

        :param size: size of column
        :param value: value of column
        :param error: error message
        :return: string, value if not None, else error in red
        """
        output = self.column(size, value) if value is not None else self.color['red'] + self.column(size,error) + self.color['nc']
        return output


    def printReport(self):
        """
        Print self.results in human readable format

        :return: None
        """
        logging.info("Printing report")
        for ip,data in self.result.items():
            mac = self.printRedifNone(20, data["mac"], "MISSING ADDRESS")
            hostname = self.printRedifNone(30, data["hostname"], "NO DNS RECORD FOUND")
            vendor = self.printRedifNone(25, None if data["vendor"] == "(Unknown)" or data["vendor"] is None else data["vendor"], "MISSING VENDOR")

            if len(data["ports"]) > 0:
                ports = []
                ports_list = ""
                for port in data["ports"]:
                    ports.append(port[0] + "/" + port[1] + "/" + port[2])
                port = None
                n = 0
                for port in ports:
                    n += 1
                    ports_list += self.column(20,port) + " "
                    if n == 3:
                        n = 0
                    if n == 0:
                        ports_list += "\n" + self.whitespace(95)

            else:
                ports_list = self.color['red'] + "NO PORTS DETECTED" + self.color['nc']

            print(self.color['nc'] + mac + vendor + self.column(20,ip) + hostname + ports_list)


    def mysqlConnection(self,options):
        """
        Connect to MySQL database server

        :return: None
        """
        try:
            logging.info("Connecting to database")
            self.mysql = mysql.connector.connect(
                host=options['host'],
                user=options['user'],
                passwd=options['pass'],
                port=options['port'],
                database=options['name']    
            )
        except Exception as e:
            logging.debug("Database connection failed, excepion thrown.", exc_info=True)
            logging.critical(str(e))
            exit(1)


    def mysqlCreateTables(self):
        """
        Create database structure

        :return: None
        """
        logging.info("Creating tables if not exists")
        c = self.mysql.cursor()
        c.execute("""
                  CREATE TABLE IF NOT EXISTS `scanresult` (
                    `ip` varchar(16) DEFAULT NULL,
                    `mac` varchar(20) DEFAULT NULL,
                    `vendor` varchar(128) DEFAULT NULL,
                    `hostname` varchar(64) DEFAULT NULL,
                    `ports` longtext,
                    `timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                    UNIQUE KEY `ip` (`ip`)
                  ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
                  """)


    def mysqlLoad(self):
        """
        Load database into self.result

        :return: None
        """
        logging.info("Loading database to result datastructure")
        c = self.mysql.cursor()
        c.execute("SELECT * FROM scanresult")
        result = c.fetchall()
        for data in result:
            self.result[data[0]] = {
                "mac": data[1],
                "vendor": data[2],
                "hostname": data[3],
                "ports": json.loads(data[4])
            }
    

    def mysqlSave(self):
        """
        Save self.result to database

        :return: None
        """
        logging.info("Saving results to database")
        c = self.mysql.cursor()
        for ip,data in self.result.items():
            sql = """
            INSERT INTO scanresult (ip, mac, vendor, hostname, ports)
            VALUES (%s, %s, %s, %s, %s)
            ON DUPLICATE KEY UPDATE
                mac = VALUES(mac),
                vendor = VALUES(vendor),
                hostname = VALUES(hostname),
                ports = VALUES(ports)
            """
            val = (ip, data['mac'], data['vendor'], data['hostname'], json.dumps(data['ports']))
            c.execute(sql, val)

        self.mysql.commit()


if __name__ == '__main__':
    # Program description
    parser = argparse.ArgumentParser(description="Scan the network(s) for active systems")

    # enforce available interfaces list, automatically checking for invalid interface name
    parser.add_argument("-i", "--interface", type=str, nargs='*', help="interface to scan", default="all",
                        choices=netifaces.interfaces())

    # get log level
    parser.add_argument('-l', '--loglevel', type=int, help="Set execution log level", default=3,
                        choices=range(0, 6))

    # Filepath
    parser.add_argument('-f', '--file', type=str, help="Load file", default=None)

    # MySQL Options
    parser.add_argument('--mysql', help="Use MySQL database", action='store_true')
    parser.add_argument('--mysql-user', type=str, help="MySQL user", default=None)
    parser.add_argument('--mysql-pass', type=str, help="MySQL password", default=None)
    parser.add_argument('--mysql-name', type=str, help="MySQL database name", default=None)
    parser.add_argument('--mysql-host', type=str, help="MySQL host", default=None)
    parser.add_argument('--mysql-port', type=int, help="MySQL port", default=3306)

    # Flags
    parser.add_argument('-u', '--update', help="Update file with new results", action='store_true')
    parser.add_argument('-p', '--printonly', help="Only print report, don't scan", action='store_true')
    parser.add_argument('-q', '--quiet', help="Disable report printing", action='store_true')

    # Get arugments
    args = parser.parse_args()

    # Enable loggings
    if args.loglevel == 0:
        logging.basicConfig(level=logging.NOTSET)
    elif args.loglevel == 1:
        logging.basicConfig(level=logging.DEBUG)
    elif args.loglevel == 2:
        logging.basicConfig(level=logging.INFO)
    elif args.loglevel == 3:
        logging.basicConfig(level=logging.WARNING)
    elif args.loglevel == 4:
        logging.basicConfig(level=logging.ERROR)
    elif args.loglevel == 5:
        logging.basicConfig(level=logging.CRITICAL)

    if args.mysql is not None and args.file is not None:
        exit("--filename and --mysql cannot be used at the same time! Make up your mind!")

    if args.quiet is True and args.file is None:
        exit("Consider writing to file, else you're just wasting resources.")

    # Init scanner
    scanner = networkscanner()

    # MySQL
    if args.mysql is True:
        scanner.mysqlConnection({
            'user': args.mysql_user,
            'pass': args.mysql_pass,
            'host': args.mysql_host,
            'port': args.mysql_port,
            'name': args.mysql_name
        })
        scanner.mysqlCreateTables()
        scanner.mysqlLoad()

    # Read file to self.results if it exists
    if args.file is not None and Path(args.file).is_file():
        with open (args.file, 'r') as f:
            data = f.read()
            try:
                logging.info("Loading file to result datastructure")
                scanner.result = json.loads(data)
                logging.info("Loaded " + str(len(scanner.result)) + " hosts")
            except Exception:
                logging.info("Not a JSON file, skipping")
                logging.debug("Exception thrown while loading JSON structure", exc_info=True)

    # Get interfaces to scan
    scanner.interfaces = args.interface

    # Init and run networkscanner
    if args.printonly is True:
        scanner.printReport()
    else:
        if os.getuid() != 0:
            exit("You need to have root privileges to scan using this script.\nPlease try again, this time using 'sudo'. Exiting.")
        scanner.run()
        if args.quiet is False:
            scanner.printReport()

    # Save to DB
    if args.mysql is True and args.printonly is False:
        scanner.mysqlSave()

    # Write result back to file if it exists
    writeFile = False
    if args.file is not None and args.printonly is False:
        if Path(args.file).is_file() is True and args.update is True:
            writeFile = True
    
        elif Path(args.file).is_file() is False:
            writeFile = True
    
        elif Path(args.file).is_file() is True and args.update is False:
            logging.warning("File exists, to save the scan result use --update")
    
    if writeFile is True:
        logging.info("Writing results to file")
        data = json.dumps(scanner.result)
        with open (args.file, 'w+') as f:
            f.write(data)
